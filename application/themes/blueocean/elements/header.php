<?php
    //defined('C5_EXECUTE') or die("Access Denied.");
    if(!defined('C5_EXECUTE') && !defined('DIR_TEMPLATE')) die('Access Denied');
    $concrete5 = defined('C5_EXECUTE');
    $opencart = defined('DIR_TEMPLATE');

    //$lang = 'en';
    $classes = array();
    $theme_path = '/application/themes/blueocean';
    $loggedin = false;
    $pagewrapper = 'page-wrapper';
    if($concrete5) {
        $u = new User();
        if($c->isEditMode()) $classes[] = 'edit';
        //if(substr_count($c->cPath,'/') == 1) $classes[] = 'home'; // multilingual
        if(substr_count($c->cPath,'/') == 1) $classes[] = 'top';
        if(is_null($c->cPath)) $classes[] = 'home';
        if($u->isLoggedIn()) $classes[] = 'loggedin';
        $theme_path = $view->getThemePath();
        $lang = Localization::activeLanguage();
        $pagewrapper = $c->getPageWrapperClass();
    }
    if($opencart) {
        $classes[] = 'opencart';
    }
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if(false): ?>
        <link rel="stylesheet" type="text/css" href="<?=$theme_path?>/css/bootstrap-modified.css">
        <?php //echo $html->css($view->getStylesheet('main.less'))?>
    <?php endif; ?>
    <?php if($concrete5): ?>
    <?php Loader::element('header_required', array('pageTitle' => isset($pageTitle) ? $pageTitle : '', 'pageDescription' => isset($pageDescription) ? $pageDescription : '', 'pageMetaKeywords' => isset($pageMetaKeywords) ? $pageMetaKeywords : ''));?>
    <?php endif; ?>
    <?php if(in_array('loggedin', $classes) || $opencart): ?>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="<?=$theme_path; ?>/js/bootstrap.min.js"></script>
    <?php endif; ?>
    <?php if($opencart): ?>
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>
        <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
        <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
        <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
    <?php endif; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style')
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto!important}'
                )
            )
            document.querySelector('head').appendChild(msViewportStyle)
        }
    </script>
    <link rel="stylesheet" type="text/css" href="<?=$theme_path; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$theme_path; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$theme_path; ?>/css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <script type="text/javascript" src="<?=$theme_path; ?>/js/js.cookie.js"></script>
    <script type="text/javascript" src="<?=$theme_path; ?>/js/grids.min.js"></script>
    <script type="text/javascript" src="<?=$theme_path; ?>/js/select2.min.js"></script>
    <script type="text/javascript" src="<?=$theme_path; ?>/js/main.js"></script>
    <?php if(false): ?>
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" id="st_insights_js" src="//w.sharethis.com/button/buttons.js?publisher=b4bb6df8-b606-44fb-8f12-d943dcaec8e6"></script>
    <script type="text/javascript">stLight.options({publisher: "b4bb6df8-b606-44fb-8f12-d943dcaec8e6", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
    <?php endif; ?>
</head>
<body class="<?=implode(' ', $classes);?>">
<div class="<?=$pagewrapper; ?>">
<?php if($opencart): ?>
    <img src="<?=$theme_path;?>/images/header.jpg" />
<?php endif; ?>