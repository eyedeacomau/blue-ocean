<?php
    //defined('C5_EXECUTE') or die("Access Denied.");
    if(!defined('C5_EXECUTE') && !defined('DIR_TEMPLATE')) die('Access Denied');
    $concrete5 = defined('C5_EXECUTE');
    $opencart = defined('DIR_TEMPLATE');
?>
<?php if($concrete5): ?>
    <footer id="ft">
        <?php
        $a = new GlobalArea('Footer Contact');
        $a->display();
        ?>
        <div class="footer-bottom container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <a class="navbar-brand" href="/"><img src="<?=$view->getThemePath(); ?>/images/logo.png" alt="Braggs Signs" /></a>
                </div>
                <div class="col-sm-4">
                <?php
                $a = new GlobalArea('Footer Middle');
                $a->display();
                ?>
                </div>
                <div class="col-sm-4">
                    <a id="back-to-top" href="#top">Back to top</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php Loader::element('footer_required'); ?>
<?php endif; ?>
<?php if($opencart): ?>
    <img src="/application/themes/blueocean/images/footer.jpg" />
</div>
<?php endif; ?>
</body>
</html>

