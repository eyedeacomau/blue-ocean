<?php defined('C5_EXECUTE') or die("Access Denied.");
//$this->inc('elements/header.php');
$root = str_replace('/staff', '', realpath('./'));
include_once($root.'/application/themes/blueocean/elements/header.php');

?>
    <header id="hd">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" id="menu-toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <button type="button" id="search-toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#search-menu" aria-expanded="false" aria-controls="search-menu">
                        <i class="fa fa-search"></i>
                    </button>
                    <a class="navbar-brand" href="/"><img src="<?=$theme_path; ?>/images/logo.png" alt="Traffix Group" /></a>
                </div>
                <?php if($edit): ?>
                    <div class="row"><div class="col-xs-12"><br />
                <?php else: ?>
                    <ul class="nav navbar-nav navbar-right">
                <?php endif; ?>
                    <?php
                    $a = new GlobalArea('Header Right');
                    $a->display();
                    ?>
                <?php if($edit): ?>
                    </div></div>
                <?php else: ?>
                    </ul>
                <?php endif; ?>
                <div id="navbar" class="navbar-collapse collapse">
                    <?php
                        $a = new GlobalArea('Menu');
                        $a->display();
                    ?>
                </div>
            </div>
        </nav>
    </header>
